#include<iostream>
#include<string>

//left hand size is the larger of the two or should be
int make_anagram(std::string rhs, std::string lhs){
  int removeCount = 0;
  //also written as *lhs.size();
  for(std::string::size_type i = 0; i < lhs.size(); i++){
    char character = lhs[i];
    bool matches = false;
    for(std::string::size_type j = 0; j < rhs.size(); j++){
      char compareCharacter = rhs[j];
      if(character == compareCharacter){
        matches = true;
      }
    }
    if(matches == false){
      removeCount++;
    }
  }
  return removeCount;
}

int set_remove_count(int removeCount, std::string longer, std::string shorter){
  if(removeCount == 0){
    return longer.size() - shorter.size();
  }
  return removeCount;
}

int main(){
  std::string stringOne = "alkfjlasdfj";
  std::string stringTwo = "llkajsdf";

  int removeCount = 0;
  if(stringOne.size() > stringTwo.size()){
    removeCount = make_anagram(stringTwo, stringOne);
    removeCount = set_remove_count(removeCount, stringOne, stringTwo);
  }
  else{
    removeCount = make_anagram(stringOne, stringTwo);
    removeCount = set_remove_count(removeCount, stringTwo, stringOne);
  }
  std::cout << "remove " << removeCount << " letters";
}
